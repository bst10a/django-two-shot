from django.urls import path
from .views import (
    receipt_listview,
    create_receipt,
    create_category,
    create_account,
    category_list,
    account_list,
)


urlpatterns = [
    path("", receipt_listview, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/", account_list, name="account_list"),
    path("accounts/create/", create_account, name="create_account")
]
